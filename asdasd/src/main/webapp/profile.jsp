<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="bootstrap.html" %>
<html>
<head>
    <title><%  %></title>
</head>
<body>
<!-- navbar -->
<%@include file="navbar.html"%>
<!-- body -->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <img src="" class="img-thumbnail" alt="Profile Photo" width="200" height="100">
        </div>
        <div class="col-md-8">
            <label for="about">O mnie:</label> <br/>
            <textarea rows="10" cols="100" readonly id="aboutme"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="col-md-4">
                <label>Wiek: </label> <br/>
                <label>Wzrost: </label> <br/>
                <label>Kolor oczu: </label> <br/>
                <label>Kolor włosów: </label> <br/>
                <label>Wykształcenie: </label> <br/>
            </div>
            <div class="col-md-4">
                <label for="who">Kogo szukam:</label> <br/>
                <textarea rows="12" cols="40" readonly id="WhoI"></textarea>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-11"></div>
        <div class="col-md-1">
            <a href="editProfile.jsp" class="btn btn-warning" role="button">Edytuj</a>
        </div>
    </div>
</div>
</body>
</html>
